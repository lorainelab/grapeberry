#!/bin/bash
#PBS -l nodes=1:ppn=4
#PBS -l vmem=32000mb
#PBS -l walltime=20:00:00
cd $PBS_O_WORKDIR
echo "SAMPLE: $SAMPLE"
echo "OUTDIR: $OUTDIR"
echo "FASTQDIR: $FASTQDIR"
tophat -p 4 -I 5000 -o $OUTDIR/$SAMPLE $GENOME $FASTQDIR/${SAMPLE}_1.fastq.gz $FASTQDIR/${SAMPLE}_2.fastq.gz
#echo $CMD
#$CMD