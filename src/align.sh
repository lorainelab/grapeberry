#!/bin/bash
# run like this: 
# align.sh 1>jobs.out 2>jobs.err
# then if you have to kill the jobs, 
# cat jobs.out | xargs qdel 
SCRIPT=runTophat.sh
D="vv_TH2.0.13"
G="genome/V_vinifera_Mar_2010"
F='fastq'
SAMPLES="T0-1 T2-51 T6-31 T12-51"
for S in $SAMPLES
do
    CMD="qsub -N $S -o $S.out -e $S.err -vSAMPLE=$S,OUTDIR=$D,GENOME=$G,FASTQDIR=$F $SCRIPT"
    $CMD
done