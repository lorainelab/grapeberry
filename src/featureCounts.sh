#!/bin/bash
SAF=SAF_for_featureCount.tsv
BASE=counts
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
featureCounts -F SAF -p -O -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
