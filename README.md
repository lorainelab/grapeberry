# Gene expression during grape berry development 

This repository contains data analysis code and data files for a
project investigating grape berry development, led María Francisca
Godoy of Pontificia Universidad Católica de Chile, Departamento de
Genética Molecular y Microbiología, Santiago, Chile. Others involved
in the project include Ann Loraine of University of North Carolina at
Charlotte and Alejandro Montenegro-Montero, also from Pontificia
Universidad Católica de Chile.

Please note that before these data are published (and peer-reviewed),
any and all files contained in this repository may change without
warning. 

* * * 

# Repository organization

Code used for analysis, processing, and figure generation are grouped
into folders with names indicating the general purpose of the
analysis. Each folder is designed to be run as a relatively
self-contained project in RStudio. As such, each folder contains an
".Rproj" file. To run the code in RStudio, just open that file and go
from there.

Some modules depend on the output of other modules. Also,
some modules depend on externally supplied data files, which are
version-controlled here but may also be available from external sites.

# What's here

## ClusterAnalysis

Groups genes into clusters using Mfuzz, from Bioconductor. Uses
FPKM file from Counts/results.

* * *

## Counts 

Makes counts, FPM, and FPKM files. Contains code for generating
barcharts showing gene expression by sample. Look here for summary of
reads or read fragments mapped to the reference genome.

* * *

## ExternalDataSets

Contains annotations downloaded from IGBQuickLoad.org and other sites.

* * *

## GeneRegions

Creates SAF (simple annotation format) data file needed by read counting program featureCounts.

* * *

## src

R and other programs used in more than one module.

* * *
 
# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* María Francisca Godoy mfgodoy@bio.puc.cl 
* Alejandro Montenegro-Montero aemonten@gmail.com

* * *

# License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT
